# Detecting sound in the library
The goal of this project is to design an embedded system for sound detection in quiet environments such as a library
## Table of Contents
* Team Members
* Simulation Using Proteus
* Document

## Team Members
* [Mona Davari](mona131376st@gmail.com)
* [Reza Pourmeshki](r.pourmeshki@gmail.com)
* [Fatemeh Farahbakhsh](fafarahbakhsh76@gmail.com)

## Simulation Using Proteus
 after compiling the code base foreach project ( main and subsidiary ) the firmware.elf or firmware.hex must be added to the source of thair fallowing boards. in addition to the above the SoundSensorLibraryTEP.HEX needs to be addes to the sound sencors.
## Document
* [google Doc](https://docs.google.com/document/d/1SmJsAqYxNEdgyWR5fQf82HMhZuW9Pb2pcFIAGlA6cK0/edit?usp=sharing)
* [Slide](https://docs.google.com/presentation/d/1IDYwLiicXZCg6ZztbgeqBciKAZNn1ZmwrNk50yOPvtc/edit?usp=sharing)
* [Simulation Video](https://drive.google.com/file/d/1nQvDsgkh4SkXLxebynoDlBdEDfr-vMtN/view)
 